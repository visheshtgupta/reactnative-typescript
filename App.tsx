import React from 'react';

import {StoreProvider} from './src/models/provider';
import {rootStore} from './src/models/rootStore.model';
import {MainRoute} from './src/navigation/rootNavigator';

export default function App() {
  return (
    <StoreProvider value={rootStore}>
      <MainRoute />
    </StoreProvider>
  );
}
