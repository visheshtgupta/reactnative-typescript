export enum ImageSize {
  Medium = 'm',
  Small = 's',
  Square = 'q',
  Large = 'b',
}
