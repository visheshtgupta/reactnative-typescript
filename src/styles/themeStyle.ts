import {StyleSheet} from 'react-native';
import {useMemo} from 'react';

import {LightColors, DarkColors, ColorType} from '../assets';
import {useStores} from '../models/provider';

const getColorStyle = (props: ColorType) =>
  StyleSheet.create({
    Primary: {
      color: props.Primary,
    },
    Secondary: {
      color: props.Secondary,
    },
    BgPrimary: {
      backgroundColor: props.BgPrimary,
    },
    BgSecondary: {
      backgroundColor: props.BgSecondary,
    },
    LightText: {
      color: props.LightText,
    },
    MediumText: {
      color: props.MediumText,
    },
    DarkText: {
      color: props.DarkText,
    },
    Border: {
      borderColor: props.Border,
    },
    BgBlur: {
      backgroundColor: props.BgBlur,
    },
  });

export const useColorStyle = () => {
  const themeStore = useStores().appTheme;
  const colors = themeStore.isDark ? DarkColors : LightColors;
  const styles = useMemo(() => getColorStyle(colors), [colors]);

  return styles;
};
