import {Api} from '../api/api';
import {API_URL_CONFIG, IMAGE_BASE_URL} from '../api/api.config';
import {ImageServiceType} from './types/image.type';

export const ImageService = {
  searchImages: (searchText: string, pageNo = 1, perPage = 10) => {
    const params = {
      text: searchText,
      sort: 'interestingness-desc',
      per_page: perPage,
      page: pageNo,
    };
    let url = API_URL_CONFIG.images.search;

    return Api.get<ImageServiceType.SearchImagesResponse>({url, params});
  },

  getImageUrl: (item?: ImageServiceType.Photo, size?: string) => {
    return item
      ? `${IMAGE_BASE_URL}/${item.server}/${item.id}_${item.secret}_${size}.jpg`
      : '';
  },
};
