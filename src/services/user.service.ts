export const UserService = {
  getUserInfo: () => {
    const userInfo = {
      name: 'Alex',
      email: 'alexend2321@gmail.com',
      imgUrl:
        'https://image.shutterstock.com/image-photo/head-shot-portrait-close-smiling-260nw-1714666150.jpg',
    };
    return userInfo;
  },
};
