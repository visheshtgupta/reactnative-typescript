export enum Icon {
  Drawer = 'md-menu-outline',
  Setting = 'ios-build',
  NoInternet = 'cellular-outline',
}
