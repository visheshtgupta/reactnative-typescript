export type ColorType = {
  Primary: string;
  Secondary: string;
  BgPrimary: string;
  BgSecondary: string;
  LightText: string;
  MediumText: string;
  DarkText: string;
  Border: string;
  BgBlur: string;
};

export const LightColors: ColorType = {
  Primary: '#0063da',
  Secondary: '#FF0284',
  BgPrimary: '#cccccc',
  BgSecondary: '#ffffff',
  LightText: '#b3b3b3',
  MediumText: '#3e3e3e',
  DarkText: '#000000',
  Border: '#767577',
  BgBlur: 'rgba(255,255,355,0.4)',
};

export const DarkColors: ColorType = {
  Primary: '#0063da',
  Secondary: '#FF0284',
  BgPrimary: '#000000',
  BgSecondary: '#231F20',
  LightText: '#767577',
  MediumText: '#b3b3b3',
  DarkText: '#ffffff',
  Border: '#767577',
  BgBlur: 'rgba(0,0,0,0.4)',
};
