export enum PrimaryFont {
  Bold = 'JosefinSans-Bold',
  Medium = 'JosefinSans-Medium',
  Regular = 'JosefinSans-Regular',
}

export enum SecondaryFont {
  Bold = 'Montserrat-Bold',
  Medium = 'Montserrat-Medium',
  Regular = 'Montserrat-Regular',
}
