export const FLICKR_API_KEY = '10952bc89141fa2bd2c0daf3e2d3d19b';

export const BASE_URL = 'https://api.flickr.com/services/rest';

export const IMAGE_BASE_URL = 'https://live.staticflickr.com';

export const API_URL_CONFIG = {
  images: {
    search: '?method=flickr.photos.search',
  },
};
