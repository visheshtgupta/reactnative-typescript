import {translate} from '../i18n';

export interface ApiError {
  code?: number | string;
  message: string;
}

export const handleApiError = (error?: ApiError) => {
  if (error?.message) {
    return {message: error.message, code: error.code};
  } else {
    return {message: translate('error.unknownError')};
  }
};
