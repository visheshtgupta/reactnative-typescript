import axios, {AxiosError} from 'axios';
import {translate} from '../i18n';
import {BASE_URL, FLICKR_API_KEY} from './api.config';
import {ApiError, handleApiError} from './api.errors';

const axiosInstance = axios.create({
  baseURL: BASE_URL,
  timeout: 10000,
  headers: {'X-Custom-Header': 'foobar'},
});

interface APIParams {
  url: string;
  Data?: Object;
  params: Object;
}

axiosInstance.interceptors.request.use(
  config => {
    config.params = {
      ...config.params,
      api_key: FLICKR_API_KEY,
      format: 'json',
      license: '4',
      extras: 'owner_name,license',
      nojsoncallback: 1,
    };
    return config;
  },
  error => {
    return Promise.reject(error);
  },
);

axiosInstance.interceptors.response.use(
  response => {
    if (response.data.stat === 'ok') {
      return response;
    } else {
      const errorResponse = handleApiError(response.data);
      return Promise.reject(errorResponse);
    }
  },
  (error: AxiosError) => {
    const errorResponse: ApiError = {
      message: error.message ?? translate('error.unknownError'),
      code: error.response?.status ?? error.status,
    };
    return Promise.reject(errorResponse);
  },
);

export const Api = {
  get: async <T>({url, params}: APIParams) => {
    return axiosInstance.get<T>(url, {params: params}).then(response => {
      return response.data;
    });
  },

  post: async <T>({url, Data}: APIParams) => {
    return axiosInstance.post<T>(url, Data).then(response => {
      return response.data;
    });
  },

  put: async <T>({url, Data}: APIParams) => {
    return axiosInstance.put<T>(url, Data).then(response => {
      return response.data;
    });
  },
};
