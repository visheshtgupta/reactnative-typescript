import i18n from 'i18n-js';

export const translate = (key: string) => {
  return key ? i18n.t(key) : '';
};
