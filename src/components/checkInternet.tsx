import NetInfo, {NetInfoState} from '@react-native-community/netinfo';
import {Animated, Easing, StyleSheet, Text} from 'react-native';
import React, {useEffect, useRef} from 'react';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {observer} from 'mobx-react-lite';

import {Icon} from '../assets';
import {translate} from '../i18n';
import {useStores} from '../models/provider';
import {useColorStyle} from '../styles/themeStyle';

export const CheckInternet: React.FC = observer(() => {
  const themeStyle = useColorStyle();
  const {isNetworkAvailable, setNetworkStatus} = useStores().networkStatus;
  const animatedValue = useRef(new Animated.Value(100)).current;

  useEffect(() => {
    Animated.timing(animatedValue, {
      toValue: 0,
      duration: 1000,
      easing: Easing.bounce,
      useNativeDriver: false,
    }).start();
  }, [animatedValue]);

  useEffect(() => {
    NetInfo.addEventListener(handleConnectivityChange);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleConnectivityChange = (state: NetInfoState) => {
    setNetworkStatus(!!state.isInternetReachable);
  };

  const renderToastView = () => {
    return (
      <Animated.View
        style={[
          styles.snackbarView,
          themeStyle.BgPrimary,
          {transform: [{translateY: animatedValue}]},
        ]}>
        <Ionicons
          name={Icon.NoInternet}
          size={20}
          color={themeStyle.Primary.color}
        />
        <Text style={[styles.snackbarText, themeStyle.DarkText]}>
          {translate('error.noInternetErrorMessage')}
        </Text>
      </Animated.View>
    );
  };

  return isNetworkAvailable ? null : renderToastView();
});

const styles = StyleSheet.create({
  snackbarView: {
    flexDirection: 'row',
    position: 'absolute',
    alignSelf: 'center',
    justifyContent: 'center',
    bottom: 30,
    width: '95%',
    borderRadius: 5,
    paddingVertical: 14,
    marginHorizontal: 16,
  },
  snackbarText: {
    alignSelf: 'center',
    fontSize: 13,
    textAlign: 'center',
    marginLeft: 5,
  },
});
