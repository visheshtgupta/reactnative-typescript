import React from 'react';
import {ActivityIndicator, ViewStyle} from 'react-native';
import {useColorStyle} from '../styles/themeStyle';
export interface LoadingProps {
  size?: 'small' | 'large';
  style?: ViewStyle;
}

export const LoadingIndicator = ({
  size = 'small',
  style = {},
}: LoadingProps) => {
  const themeStyle = useColorStyle();
  return (
    <ActivityIndicator
      color={themeStyle.Primary.color}
      animating={true}
      size={size}
      style={style}
    />
  );
};
