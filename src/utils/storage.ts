import AsyncStorage from '@react-native-async-storage/async-storage';

export const Storage = {
  storeData: async (key: string, value: string): Promise<boolean> => {
    try {
      await AsyncStorage.setItem(key, value);

      return true;
    } catch (e) {
      return false;
    }
  },

  getData: async (key: string) => {
    try {
      const value = await AsyncStorage.getItem(key);

      if (value !== null) {
        return value;
      }
    } catch (e) {
      return null;
    }
  },
};
