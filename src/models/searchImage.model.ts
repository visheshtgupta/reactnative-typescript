import {cast, flow, types} from 'mobx-state-tree';

import {ApiError} from '../api/api.errors';
import {ImageService} from '../services/image.service';

const Imagesdata = types.model({
  id: types.string,
  owner: types.string,
  secret: types.string,
  server: types.string,
  farm: types.number,
  title: types.string,
  ispublic: types.number,
  isfriend: types.number,
  isfamily: types.number,
  license: types.string,
  ownername: types.string,
});

export const SearchImageModel = types
  .model({
    images: types.array(Imagesdata),
    currPage: types.number,
    totalPage: types.number,
    errorMessage: types.string,
  })
  .actions(self => ({
    searchImages: flow(function* searchImages(
      searchText: string,
      loadMore: boolean,
    ) {
      try {
        const response = yield ImageService.searchImages(
          searchText,
          loadMore ? self.currPage + 1 : 1,
        );
        self.images = loadMore
          ? self.images.concat(response.photos.photo)
          : response.photos.photo;
        self.currPage = response.photos.page;
        self.totalPage = response.photos.pages;
      } catch (error) {
        console.log('Error --', error);
        const err = error as ApiError;
        self.errorMessage = err.message;
      }
    }),
    resetStore() {
      self.images = cast([]);
      self.currPage = 0;
      self.totalPage = 1;
    },

    resetError() {
      self.errorMessage = '';
    },
  }))
  .views(self => ({
    findImage(id: string) {
      return self.images.find(image => image.id === id);
    },
  }));
