import {types} from 'mobx-state-tree';

export const AppThemeModel = types
  .model({
    isDark: types.boolean,
  })
  .actions(self => ({
    setTheme(value: boolean) {
      self.isDark = value;
    },
  }));
