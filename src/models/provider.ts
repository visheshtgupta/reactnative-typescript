import {createContext, useContext} from 'react';
import {RootStore} from './rootStore.model';

const StoreContext = createContext<RootStore>({} as RootStore);

export const StoreProvider = StoreContext.Provider;

export const useStores = () => useContext(StoreContext);
