import AsyncStorage from '@react-native-async-storage/async-storage';
import {Instance, types} from 'mobx-state-tree';
import {persist} from 'mst-persist';

import {NetworkStatusModel} from './networkStatus.model';
import {SearchImageModel} from './searchImage.model';
import {AppThemeModel} from './appTheme.model';

export const RootStoreModel = types.model('RootStore', {
  searchImage: types.optional(SearchImageModel, {
    images: [],
    currPage: 0,
    totalPage: 1,
    errorMessage: '',
  }),
  appTheme: types.optional(AppThemeModel, {isDark: false}),
  networkStatus: types.optional(NetworkStatusModel, {
    isNetworkAvailable: false,
  }),
});

export const rootStore = RootStoreModel.create();

export const initializeStore = () => {
  return persist('RootPersist', rootStore, {
    storage: AsyncStorage,
    jsonify: true,
    whitelist: ['appTheme'],
  });
};

export interface RootStore extends Instance<typeof RootStoreModel> {}
