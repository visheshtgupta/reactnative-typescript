import {types} from 'mobx-state-tree';

export const NetworkStatusModel = types
  .model({
    isNetworkAvailable: types.boolean,
  })
  .actions(self => ({
    setNetworkStatus: (status: boolean) => {
      self.isNetworkAvailable = status;
    },
  }));
