import {NativeStackScreenProps} from '@react-navigation/native-stack';
import React, {useEffect} from 'react';
import {View, StyleSheet} from 'react-native';
import SplashScreen from 'react-native-splash-screen';

import {initializeStore} from '../../models/rootStore.model';
import {RootName, RootParamList} from '../../navigation/types';

type SplashProps = NativeStackScreenProps<RootParamList, RootName.SplashScreen>;

const SPLASH_TIME_OUT: number = 1200;

export const Splash = (props: SplashProps) => {
  useEffect(() => {
    initializeStore().then(() => {
      setTimeout(() => {
        SplashScreen.hide();
        props.navigation.replace(RootName.DrawerNavigator);
      }, SPLASH_TIME_OUT);
    });

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return <View style={styles.flex} />;
};

const styles = StyleSheet.create({
  flex: {
    flex: 1,
  },
});
