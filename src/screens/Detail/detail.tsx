import {StyleSheet, Image, Text} from 'react-native';
import React, {useEffect, useState} from 'react';

import {PrimaryFont} from '../../assets';
import {ImageSize} from '../../constants/enum';
import {useStores} from '../../models/provider';
import {ImageService} from '../../services/image.service';
import {ImageServiceType} from '../../services/types/image.type';
import {useColorStyle} from '../../styles/themeStyle';
import {DetailProps} from './type';

export const Detail = (props: DetailProps) => {
  const themeStyle = useColorStyle();
  const imageStore = useStores().searchImage;
  const [url, setUrl] = useState<string>();
  const [image, setImage] = useState<ImageServiceType.Photo>();

  useEffect(() => {
    const item = imageStore.findImage(props.route.params.id);
    const getUrl = ImageService.getImageUrl(item, ImageSize.Large);
    setUrl(getUrl);
    setImage(item);

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      <Image style={styles.image} source={{uri: url}} />
      <Text style={[styles.title, themeStyle.DarkText]}>{image?.title}</Text>
    </>
  );
};

const styles = StyleSheet.create({
  image: {
    height: '60%',
    width: '100%',
    justifyContent: 'flex-end',
  },
  title: {
    fontSize: 22,
    alignSelf: 'flex-start',
    marginBottom: 40,
    marginLeft: 26,
    marginTop: 16,
    fontFamily: PrimaryFont.Bold,
  },
});
