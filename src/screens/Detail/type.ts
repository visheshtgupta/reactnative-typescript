import {NativeStackScreenProps} from '@react-navigation/native-stack';

import {RootName, RootParamList} from '../../navigation/types';

export type DetailProps = NativeStackScreenProps<
  RootParamList,
  RootName.DetailScreen
>;
