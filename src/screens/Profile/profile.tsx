import {StyleSheet, Text, View, Image} from 'react-native';
import React from 'react';

import {PrimaryFont} from '../../assets';
import {useColorStyle} from '../../styles/themeStyle';
import {UserService} from '../../services/user.service';

export const Profile = () => {
  const themeStyle = useColorStyle();
  const userData = UserService.getUserInfo();

  return (
    <View style={[styles.container, themeStyle.BgSecondary]}>
      <Image
        style={styles.image}
        source={{
          uri: userData.imgUrl,
        }}
      />
      <Text style={[styles.title, themeStyle.DarkText]}>{userData.name}</Text>
      <Text style={[styles.email, themeStyle.MediumText]}>
        {userData.email}
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  image: {
    height: 140,
    width: 140,
    borderRadius: 100,
    alignSelf: 'center',
    marginTop: 18,
  },
  title: {
    fontSize: 22,
    alignSelf: 'center',
    marginTop: 20,
    fontFamily: PrimaryFont.Bold,
  },
  email: {
    fontSize: 16,
    alignSelf: 'center',
    marginTop: 10,
    fontFamily: PrimaryFont.Regular,
  },
});
