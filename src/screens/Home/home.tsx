import React, {useCallback, useState} from 'react';
import {
  View,
  Text,
  TextInput,
  StyleSheet,
  Image,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import Animated, {
  useAnimatedStyle,
  useSharedValue,
  withTiming,
} from 'react-native-reanimated';
import {debounce} from 'lodash';
import {observer} from 'mobx-react-lite';

import {PrimaryFont} from '../../assets';
import {LoadingIndicator} from '../../components/loadingIndicator';
import {ImageSize} from '../../constants/enum';
import {translate} from '../../i18n';
import {useStores} from '../../models/provider';
import {RootName} from '../../navigation/types';
import {ImageService} from '../../services/image.service';
import {ImageServiceType} from '../../services/types/image.type';
import {useColorStyle} from '../../styles/themeStyle';
import {HomeProps} from './type';

export const Home = observer((props: HomeProps) => {
  const themeStyle = useColorStyle();
  const imageStore = useStores().searchImage;
  const networkStatus = useStores().networkStatus;
  const [loading, setLoading] = useState(false);
  const [searchText, setSearchText] = useState('');
  const bgOpacity = useSharedValue(0);
  const searchHeaderMargin = useSharedValue(0);
  const [zoomImageData, setZoomImageData] = useState({
    show: false,
    currIndex: 0,
    imgUrl: '',
  });

  const zoomImageStyle = useAnimatedStyle(() => ({
    opacity: withTiming(bgOpacity.value, {
      duration: 100,
    }),
  }));

  const searchHeaderStyle = useAnimatedStyle(() => ({
    marginTop: withTiming(searchHeaderMargin.value),
  }));

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const debounceSearchImages = useCallback(
    debounce(text => searchImage(text, false), 500),
    [],
  );

  const searchImage = async (text: string | undefined, loadMore: boolean) => {
    if (
      networkStatus.isNetworkAvailable &&
      !loading &&
      imageStore.currPage <= imageStore.totalPage &&
      text
    ) {
      setLoading(true);
      await imageStore.searchImages(text, loadMore);
      setLoading(false);
    }
  };

  const onLongPressHandler = (url: string, index: number) => {
    bgOpacity.value = withTiming(1, {duration: 300});
    setZoomImageData({
      show: true,
      currIndex: index,
      imgUrl: url,
    });
  };

  const onPressOutHandler = () => {
    bgOpacity.value = withTiming(0);
    setZoomImageData({
      show: false,
      currIndex: -1,
      imgUrl: '',
    });
  };

  const renderImageItem = ({
    item,
    index,
  }: {
    item: ImageServiceType.Photo;
    index: number;
  }) => {
    const url = ImageService.getImageUrl(item, ImageSize.Medium);

    return (
      <TouchableOpacity
        delayLongPress={100}
        onLongPress={() => onLongPressHandler(url, index)}
        onPressOut={onPressOutHandler}
        style={[themeStyle.BgSecondary, styles.itemView]}
        onPress={() =>
          props.navigation.navigate(RootName.DetailScreen, {
            id: item.id,
          })
        }>
        <Image
          style={styles.img}
          source={{
            uri: url,
          }}
        />
        <Text style={[styles.imageItemText, themeStyle.DarkText]}>
          {item.title ?? translate('screens.home.defaultImageTitle')}
        </Text>
        <Text style={[styles.imageItemDesc, themeStyle.LightText]}>
          {item.ownername ?? translate('screens.home.defaultImageDesc')}
        </Text>
      </TouchableOpacity>
    );
  };

  const renderPrimaryButton = (load: boolean) => {
    return (
      <View style={!load ? styles.primaryButton : null}>
        <Text style={[styles.errText, themeStyle.DarkText]}>
          {imageStore.errorMessage}
        </Text>
        <TouchableOpacity onPress={() => searchImage(searchText, load)}>
          <View style={[styles.loadErrorView, themeStyle.BgSecondary]}>
            <Text style={[styles.loadErrorText, themeStyle.DarkText]}>
              {translate('common.retry')}
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  };

  const onSearchText = (text: string) => {
    imageStore.resetStore();
    imageStore.resetError();
    setSearchText(text);
    debounceSearchImages(text);
  };

  const renderSearchtext = () => {
    return (
      <Animated.View style={searchHeaderStyle}>
        <TextInput
          placeholder={translate('screens.home.searchImage')}
          style={[
            styles.search,
            themeStyle.Border,
            themeStyle.MediumText,
            themeStyle.BgSecondary,
          ]}
          onChangeText={onSearchText}
          placeholderTextColor={themeStyle.LightText.color}
        />
      </Animated.View>
    );
  };

  const renderFooterComponent = () => {
    if (imageStore.errorMessage && imageStore.currPage > 0) {
      return renderPrimaryButton(true);
    } else if (loading && imageStore.images.length > 0) {
      return <LoadingIndicator style={styles.ListFooterLoader} size="large" />;
    } else {
      return null;
    }
  };

  const renderEmptyComponent = () => {
    return (
      <View style={styles.itemView}>
        <Text style={[styles.noItemText, themeStyle.DarkText]}>
          {translate('screens.home.noItemFound')}
        </Text>
      </View>
    );
  };

  const renderSearchResult = () => {
    return (
      <FlatList
        onScroll={event => {
          const scrolling = event.nativeEvent.contentOffset.y;
          if (scrolling > 100) {
            searchHeaderMargin.value = withTiming(-50);
          } else {
            searchHeaderMargin.value = withTiming(0);
          }
        }}
        scrollEventThrottle={16}
        showsVerticalScrollIndicator={false}
        numColumns={2}
        initialNumToRender={10}
        columnWrapperStyle={[styles.columnWrapperStyle, themeStyle.BgPrimary]}
        data={imageStore.images}
        renderItem={renderImageItem}
        style={[styles.flatlist, themeStyle.BgSecondary]}
        onEndReached={() => searchImage(searchText, true)}
        onEndReachedThreshold={0.4}
        ListFooterComponent={renderFooterComponent}
        ListEmptyComponent={
          imageStore.currPage > 0 ? renderEmptyComponent : null
        }
        ItemSeparatorComponent={() => (
          <View style={[styles.separator, themeStyle.BgPrimary]} />
        )}
      />
    );
  };

  const renderMainLoader = () => {
    if (loading && imageStore.images.length < 1) {
      return (
        <View style={styles.loadingView}>
          <LoadingIndicator style={styles.mainLoader} size="large" />
        </View>
      );
    } else {
      return null;
    }
  };

  const renderZoomImage = () => {
    return (
      <Animated.View
        style={[styles.zoomImageView, zoomImageStyle, themeStyle.BgBlur]}>
        <Image
          style={styles.zoomImage}
          source={{
            uri: zoomImageData.imgUrl,
          }}
        />
      </Animated.View>
    );
  };

  return (
    <View style={[styles.container, themeStyle.BgPrimary]}>
      {renderSearchtext()}
      {imageStore.errorMessage && imageStore.currPage < 1
        ? renderPrimaryButton(false)
        : renderSearchResult()}
      {renderMainLoader()}
      {zoomImageData.show ? renderZoomImage() : null}
    </View>
  );
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  imageItemText: {
    fontSize: 16,
    textAlign: 'center',
    fontFamily: PrimaryFont.Bold,
    marginTop: 3,
    width: 155,
  },
  imageItemDesc: {
    fontSize: 14,
    textAlign: 'center',
    fontFamily: PrimaryFont.Regular,
    marginTop: 8,
    width: 155,
  },
  flatlist: {
    marginTop: 10,
  },
  itemView: {
    padding: 10,
  },
  columnWrapperStyle: {
    padding: 10,
    justifyContent: 'space-between',
  },
  ListFooterLoader: {
    marginVertical: 40,
  },
  mainLoader: {
    justifyContent: 'center',
    alignSelf: 'center',
  },
  img: {
    height: 160,
    width: 160,
  },
  zoomImageView: {
    height: '100%',
    width: '100%',
    position: 'absolute',
    zIndex: 1,
  },
  zoomImage: {
    top: '30%',
    height: '40%',
    width: '100%',
  },
  search: {
    borderWidth: 1,
    padding: 10,
    fontSize: 18,
  },
  noItemText: {
    textAlign: 'center',
    fontSize: 15,
    fontFamily: PrimaryFont.Bold,
  },
  loadErrorView: {
    width: '40%',
    height: 40,
    borderRadius: 20,
    justifyContent: 'center',
    alignSelf: 'center',
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 50,
  },
  loadErrorText: {
    textAlign: 'center',
    fontSize: 16,
    fontFamily: PrimaryFont.Bold,
    marginTop: 5,
    fontWeight: '800',
  },
  loadingView: {
    justifyContent: 'center',
    height: '100%',
  },
  primaryButton: {
    justifyContent: 'center',
    height: '100%',
  },
  errText: {
    marginTop: 20,
    textAlign: 'center',
    fontSize: 16,
    fontWeight: '500',
  },
  separator: {
    height: 10,
  },
});
