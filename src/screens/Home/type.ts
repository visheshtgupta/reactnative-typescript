import {NativeStackScreenProps} from '@react-navigation/native-stack';
import {RootName, RootParamList} from '../../navigation/types';
import {ImageServiceType} from '../../services/types/image.type';

export type HomeProps = NativeStackScreenProps<
  RootParamList,
  RootName.HomeScreen
>;

export interface ImageDataProps {
  images: ImageServiceType.Photo;
  currPage: number;
  totalPage: number;
}
