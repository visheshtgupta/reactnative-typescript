import {StyleSheet, Text, View, Switch} from 'react-native';
import React, {useState} from 'react';
import Ionicons from 'react-native-vector-icons/Ionicons';

import {Icon, LightColors} from '../../assets';
import {translate} from '../../i18n';
import {useStores} from '../../models/provider';
import {useColorStyle} from '../../styles/themeStyle';

export const Setting = () => {
  const themeStyle = useColorStyle();
  const theme = useStores().appTheme;
  const [scheme, setScheme] = useState(theme.isDark);

  const toggleSwitch = () => {
    theme.setTheme(!theme.isDark);
    setScheme(theme.isDark);
  };

  return (
    <View style={themeStyle.BgSecondary}>
      <View style={[styles.titleView, themeStyle.Border]}>
        <Ionicons
          style={styles.icon}
          name={Icon.Setting}
          size={24}
          color={themeStyle.Primary.color}
        />
        <Text style={[styles.title, themeStyle.DarkText]}>
          {translate('screens.setting.appearance')}
        </Text>
      </View>
      <View style={styles.subTitleView}>
        <Text style={[styles.subTitle, themeStyle.DarkText]}>
          {translate('screens.setting.theme')}
        </Text>
        <Switch
          thumbColor={LightColors.BgSecondary}
          trackColor={{true: LightColors.Primary}}
          onValueChange={toggleSwitch}
          value={scheme}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  titleView: {
    flexDirection: 'row',
    padding: 10,
    alignItems: 'center',
    borderBottomWidth: 1,
  },
  icon: {
    paddingHorizontal: 10,
  },
  title: {
    fontSize: 18,
    marginLeft: 10,
    fontWeight: '500',
  },
  subTitleView: {
    flexDirection: 'row',
    padding: 10,
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  subTitle: {
    fontSize: 16,
    marginHorizontal: 10,
  },
});
