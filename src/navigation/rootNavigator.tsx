import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {observer} from 'mobx-react-lite';
import React from 'react';
import {SafeAreaProvider} from 'react-native-safe-area-context';

import {CheckInternet} from '../components/checkInternet';
import {DrawerNavigator} from './drawer/drawerNavigator';
import {useStores} from '../models/provider';
import {NavigationDarkTheme, NavigationLightTheme} from './navigationThemes';
import {Splash} from '../screens/Splash/splash';
import {Detail} from '../screens/Detail/detail';
import {RootName, RootParamList} from './types';

const Stack = createNativeStackNavigator<RootParamList>();

const RootStack = () => {
  return (
    <Stack.Navigator
      initialRouteName={RootName.SplashScreen}
      screenOptions={{headerShown: false, gestureEnabled: true}}>
      <Stack.Screen name={RootName.SplashScreen} component={Splash} />
      <Stack.Screen
        options={{headerShown: true}}
        name={RootName.DetailScreen}
        component={Detail}
      />
      <Stack.Screen
        name={RootName.DrawerNavigator}
        component={DrawerNavigator}
      />
    </Stack.Navigator>
  );
};

export const MainRoute = observer(() => {
  const themeStore = useStores().appTheme;

  return (
    <SafeAreaProvider>
      <NavigationContainer
        theme={themeStore.isDark ? NavigationDarkTheme : NavigationLightTheme}>
        <RootStack />
      </NavigationContainer>
      <CheckInternet />
    </SafeAreaProvider>
  );
});
