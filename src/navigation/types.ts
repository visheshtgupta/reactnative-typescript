export type RootParamList = {
  [RootName.SplashScreen]: undefined;
  [RootName.HomeScreen]: undefined;
  [RootName.DetailScreen]: {id: string};
  [RootName.DrawerNavigator]: undefined;
};

export enum RootName {
  SplashScreen = 'Splash',
  HomeScreen = 'Home',
  DetailScreen = 'Detail',
  DrawerNavigator = 'Drawer',
}
