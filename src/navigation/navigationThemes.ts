import {DarkColors, LightColors} from '../assets/colors';

export const NavigationLightTheme = {
  dark: false,
  colors: {
    primary: LightColors.Primary,
    background: LightColors.BgPrimary,
    card: LightColors.BgPrimary,
    text: LightColors.DarkText,
    border: LightColors.Border,
    notification: LightColors.Secondary,
  },
};

export const NavigationDarkTheme = {
  dark: true,
  colors: {
    primary: DarkColors.Primary,
    background: DarkColors.BgPrimary,
    card: DarkColors.BgPrimary,
    text: DarkColors.DarkText,
    border: DarkColors.Border,
    notification: DarkColors.Secondary,
  },
};
