import {
  DrawerContentScrollView,
  DrawerItemList,
  DrawerContentComponentProps,
} from '@react-navigation/drawer';
import React from 'react';
import {Image, StyleSheet, Text} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';

import {PrimaryFont} from '../../assets';
import {UserService} from '../../services/user.service';
import {useColorStyle} from '../../styles/themeStyle';

export const DrawerContent = (props: DrawerContentComponentProps) => {
  const themeStyle = useColorStyle();
  const userData = UserService.getUserInfo();

  return (
    <SafeAreaView style={styles.container}>
      <Image
        style={styles.image}
        source={{
          uri: userData.imgUrl,
        }}
      />
      <Text style={[styles.title, themeStyle.DarkText]}>{userData.name}</Text>
      <Text style={[styles.email, themeStyle.DarkText]}>{userData.email}</Text>
      <DrawerContentScrollView {...props}>
        <DrawerItemList {...props} />
      </DrawerContentScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  image: {
    height: 140,
    width: 140,
    borderRadius: 100,
    alignSelf: 'center',
    marginTop: 8,
  },
  title: {
    fontSize: 22,
    alignSelf: 'center',
    marginTop: 16,
    fontFamily: PrimaryFont.Bold,
  },
  email: {
    fontSize: 14,
    alignSelf: 'center',
    marginTop: 2,
    fontFamily: PrimaryFont.Regular,
  },
});
