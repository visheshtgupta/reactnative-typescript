export type DrawerParamList = {
  [DrawerName.HomeScreen]: undefined;
  [DrawerName.ProfileScreen]: undefined;
  [DrawerName.SettingScreen]: undefined;
};

export enum DrawerName {
  HomeScreen = 'Home',
  ProfileScreen = 'Profile',
  SettingScreen = 'Setting',
}
