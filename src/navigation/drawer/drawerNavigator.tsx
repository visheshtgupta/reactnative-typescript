import {
  createDrawerNavigator,
  DrawerHeaderProps,
} from '@react-navigation/drawer';
import {DrawerActions} from '@react-navigation/native';
import React from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';

import {Icon} from '../../assets';
import {DrawerContent} from './drawerContent';
import {Home} from '../../screens/Home/home';
import {Profile} from '../../screens/Profile/profile';
import {Setting} from '../../screens/Setting/setting';
import {useColorStyle} from '../../styles/themeStyle';
import {DrawerName, DrawerParamList} from './types';

export const Drawer = createDrawerNavigator<DrawerParamList>();

export const DrawerNavigator = (props: DrawerHeaderProps) => {
  const themeStyle = useColorStyle();

  const renderHeaderRight = () => {
    return (
      <TouchableOpacity
        onPress={() => props.navigation.dispatch(DrawerActions.openDrawer())}>
        <Ionicons
          style={styles.icon}
          name={Icon.Drawer}
          size={24}
          color={themeStyle.Primary.color}
        />
      </TouchableOpacity>
    );
  };

  const renderHeaderLeft = () => {
    return null;
  };

  return (
    <Drawer.Navigator
      screenOptions={{
        drawerPosition: 'right',
        headerLeft: renderHeaderLeft,
        headerRight: renderHeaderRight,
      }}
      drawerContent={prop => <DrawerContent {...prop} />}>
      <Drawer.Screen name={DrawerName.HomeScreen} component={Home} />
      <Drawer.Screen
        name={DrawerName.ProfileScreen}
        component={Profile}
        options={{unmountOnBlur: true}}
      />
      <Drawer.Screen name={DrawerName.SettingScreen} component={Setting} />
    </Drawer.Navigator>
  );
};

const styles = StyleSheet.create({
  icon: {
    paddingRight: 10,
  },
});
